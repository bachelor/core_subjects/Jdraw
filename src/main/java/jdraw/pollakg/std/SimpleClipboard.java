package jdraw.pollakg.std;

import java.util.List;
import java.util.LinkedList;

import jdraw.framework.Figure;

/**
 * @author Georg Pollak 	
 */

public final class SimpleClipboard {
        private SimpleClipboard() { }
        private static List<Figure> figures = new LinkedList<Figure>();

        public static void add(Figure f) {
                figures.add(f);
        }

        public static void addAll(List<Figure> set) {
                figures.addAll(set);
        }

        public static List<Figure> get() {
                return new LinkedList<Figure>(figures);
        }

        public static void clear() {
                figures.clear();
        }
}
