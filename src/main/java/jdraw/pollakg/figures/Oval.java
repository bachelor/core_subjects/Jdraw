package jdraw.pollakg.figures;

import java.awt.geom.Ellipse2D;	

import jdraw.pollakg.figures.Oval;

import java.awt.Point;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * Represents ellipses in JDraw.
 * 
 * @author Georg Pollak
 *
 */
public class Oval extends AbstractRectangularFigure {
	
	/**
	 * Create a new ellipse of the given dimension.
	 * @param x the x-coordinate of the upper left corner of the rectangular box of the ellipse
	 * @param y the y-coordinate of the upper left corner of the rectangular box of the ellipse
	 * @param w the ellipse's width
	 * @param h the ellipse's height
	 */
	public Oval(int x, int y, int w, int h) {
		super(new Ellipse2D.Float(x, y, w, h));
	}

    Oval(Point p) {
        this(p.x, p.y, 0, 0);
    }

    Oval(Oval o) {
        this((int) o.shape.getX(), (int) o.shape.getY(), (int) o.shape.getWidth(), (int) o.shape.getHeight());
    }
	/**
	 * Draw the ellipse to the given graphics context.
	 * @param g the graphics context to use for drawing.
	 */
	@Override
	public void draw(Graphics g) {
		// Attention: must be Rectangle here
		Rectangle e = getBounds();
		g.setColor(Color.WHITE);
		g.fillOval((int) e.x, (int) e.y, (int) e.width, (int) e.height);
		g.setColor(Color.BLACK);
		g.drawOval((int) e.x, (int) e.y, (int) e.width, (int) e.height);
	}

    @Override
    public Oval clone() {
            return new Oval(this);
    }
}
