package jdraw.pollakg.figures;

import java.awt.Point;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import jdraw.framework.DrawContext;
import jdraw.framework.DrawView;

/**
 * This tool defines a mode for drawing rectangles.
 *
 * @see jdraw.framework.Figure
 *
 * @author Georg Pollak
 */
public class RectTool extends AbstractFigureTool {

	/**
	 * Create a new rectangle tool for the given context.
	 * @param context a context to use this tool in.
	 */
	public RectTool(DrawContext context) {
        super(context);
    }
	
	@Override
	public Icon getIcon() {
		return new ImageIcon(getClass().getResource(IMAGES + "rectangle.png"));
	}

	@Override
	public String getName() {
		return "Rectangle";
	}

    @Override
    protected AbstractFigure createFigure(Point p) {
        return new Rect(p);
    }

}
