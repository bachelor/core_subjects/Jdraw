package jdraw.pollakg.figures;

import jdraw.pollakg.figures.handles.*;	

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Collections;

import java.awt.Point;
import java.awt.Graphics;
import java.awt.Rectangle;

import jdraw.framework.Figure;
import jdraw.framework.FigureGroup;
import jdraw.framework.FigureHandle;

/**
 * @author Georg Pollak
 */

@SuppressWarnings("serial")
public class GroupFigure extends AbstractFigure implements FigureGroup {

        private List<Figure> parts;
        private List<FigureHandle> handles = new ArrayList<>(8);

        public GroupFigure(List<Figure> setlectedFigures) {
    		// add figures to group with shallow copy
            parts = new LinkedList<Figure>(setlectedFigures);
            // add figure handles
            handles.add(new Handle(new NWHandle(this)));
            handles.add(new Handle(new NEHandle(this)));
            handles.add(new Handle(new SWHandle(this)));
            handles.add(new Handle(new SEHandle(this)));
            handles.add(new Handle(new NHandle(this)));
            handles.add(new Handle(new EHandle(this)));
            handles.add(new Handle(new SHandle(this)));
            handles.add(new Handle(new WHandle(this)));
        }

        @Override
        public Iterable<Figure> getFigureParts() {
                return Collections.unmodifiableList(parts);
        }

        @Override
        public void draw(Graphics g) {
                for (Figure f: parts) {
                        f.draw(g);
                }
        }

        @Override
        public void setBounds(Point origin, Point corner) {
        }

        @Override
        public void move(int dx, int dy) {
                if (dx != 0 || dy != 0) {
                        for (Figure f: parts) {
                                f.move(dx, dy);
                        }
                }
                notifyAllListeners();
        }

        @Override
        public boolean contains(int x, int y) {
                return getBounds().contains(x, y);
        }

        @Override
        public Rectangle getBounds() {
                Rectangle bounds = null;
                for (Figure f: parts) {
                        if (bounds == null) {
                        	// inital bounding box
                                bounds = f.getBounds();
                        } else {
                                bounds.add(f.getBounds());
                        }
                }
                return bounds;
        }

        @Override
        public List<FigureHandle> getHandles() {
                return handles;
        }

        @Override
        public GroupFigure clone() {
                GroupFigure c = new GroupFigure(parts);
                c.parts = new LinkedList<Figure>();
                for (Figure f: parts) {
                        c.parts.add(f.clone());
                }
                return c;
        }

}
