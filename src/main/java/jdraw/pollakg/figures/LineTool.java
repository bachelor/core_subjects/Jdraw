package jdraw.pollakg.figures;

import java.awt.Point;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import jdraw.framework.DrawContext;
import jdraw.framework.DrawView;

/**
 * This tool defines a mode for drawing lines.
 *
 * @see jdraw.framework.Figure
 *
 * @author Georg Pollak
 */
public class LineTool extends AbstractFigureTool {
  
	/**
	 * Create a new line tool for the given context.
	 * @param context a context to use this tool in.
	 */
	public LineTool(DrawContext context) {
        super(context);
	}
	
	@Override
	public Icon getIcon() {
		return new ImageIcon(getClass().getResource(IMAGES + "line.png"));
	}

	@Override
	public String getName() {
		return "Line";
	}

    @Override
    protected AbstractFigure createFigure(Point p) {
        return new Line(p);
    }

}
