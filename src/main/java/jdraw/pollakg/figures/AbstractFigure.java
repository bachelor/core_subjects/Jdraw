package jdraw.pollakg.figures;

import java.awt.Point;
import java.awt.Graphics;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jdraw.framework.Figure;
import jdraw.framework.FigureEvent;
import jdraw.framework.FigureHandle;
import jdraw.framework.FigureListener;

/**
 * @author Georg Pollak
 */

public abstract class AbstractFigure implements Figure {

    private List<FigureListener> listeners = new CopyOnWriteArrayList<>();

    protected void notifyAllListeners() {
        for (FigureListener l : listeners) {
            l.figureChanged(new FigureEvent(this));
        }
    }

    @Override
    public abstract void draw(Graphics g);

    @Override
    public abstract void setBounds(Point origin, Point corner);

    @Override
    public abstract void move(int dx, int dy);

    @Override
    public abstract boolean contains(int x, int y);

	/**
	 * Returns a list of handles for this Figure.
	 * @return all handles that are attached to the targeted figure.
	 * @see jdraw.framework.Figure#getHandles()
	 */	
    public abstract List<FigureHandle> getHandles();

    @Override
    public void addFigureListener(FigureListener listener) {
        if (listener != null && !listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeFigureListener(FigureListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    @Override
    public Figure clone() {
            try {
                    return (Figure) super.clone();
            } catch (CloneNotSupportedException e) {
                    throw new InternalError();
            }
    }

}
