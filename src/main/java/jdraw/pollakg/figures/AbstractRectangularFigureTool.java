package jdraw.pollakg.figures;

import java.awt.Point;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import jdraw.framework.DrawContext;
import jdraw.framework.DrawView;

/**
 * This tool defines a mode for drawing AbstractRectangularFigures.
 *
 * @see jdraw.framework.Figure
 *
 * @author  Georg Pollak
 */
public abstract class AbstractRectangularFigureTool extends AbstractFigureTool {

	/**
	 * Create a new shape tool for the given context.
	 * @param context a context to use this tool in.
	 */
	public AbstractRectangularFigureTool(DrawContext context) {
        super(context);
	}

	@Override
	public abstract Icon getIcon();

	@Override
	public abstract String getName();

    @Override
    protected abstract AbstractFigure createFigure(Point p);
}
