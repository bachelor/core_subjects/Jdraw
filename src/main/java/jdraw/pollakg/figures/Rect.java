/*
 * Copyright (c) 2017 Fachhochschule Nordwestschweiz (FHNW)
 * All Rights Reserved. 
 */

package jdraw.pollakg.figures;

import java.awt.Color;		
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;

import jdraw.framework.DrawModelEvent;
import jdraw.framework.Figure;
// In order to add figure events
// Expects as parameter the source of the figure event 
// Has method get to access figure that triggered event
import jdraw.framework.FigureEvent;
import jdraw.framework.FigureHandle;
import jdraw.framework.FigureListener;
import jdraw.framework.DrawModelEvent.Type;
import jdraw.pollakg.figures.AbstractRectangularFigure;
import jdraw.pollakg.figures.Rect;

/**
 * Represents rectangles in JDraw.
 * 
 * @author Georg Pollak
 *
 */
@SuppressWarnings("serial")
public class Rect extends AbstractRectangularFigure {
	
	/**
	 * Create a new rectangle of the given dimension.
	 * @param x the x-coordinate of the upper left corner of the rectangle
	 * @param y the y-coordinate of the upper left corner of the rectangle
	 * @param w the rectangle's width
	 * @param h the rectangle's height
	 */
	public Rect(int x, int y, int w, int h) {
		super(new Rectangle(x, y, w, h));
	}

    Rect(Point p) {
        this(p.x, p.y, 0, 0);
    }
    
    Rect(Rect r) {
        this((int) r.shape.getX(), (int) r.shape.getY(), (int) r.shape.getWidth(), (int) r.shape.getHeight());
    }

	/**
	 * Draw the rectangle to the given graphics context.
	 * @param g the graphics context to use for drawing.
	 */
	@Override
	public void draw(Graphics g) {
		// see alternative implementation inside Oval
		g.setColor(Color.WHITE);
		g.fillRect(((Rectangle) shape).x, ((Rectangle) shape).y, ((Rectangle) shape).width, ((Rectangle) shape).height);
		g.setColor(Color.BLACK);
		g.drawRect(((Rectangle) shape).x, ((Rectangle) shape).y, ((Rectangle) shape).width, ((Rectangle) shape).height);
	}

    @Override
    public Rect clone() {
            return new Rect(this);
    }
}