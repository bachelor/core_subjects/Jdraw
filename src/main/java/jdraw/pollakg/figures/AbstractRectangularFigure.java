package jdraw.pollakg.figures;

import java.awt.geom.RectangularShape;					

import java.awt.Point;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.List;
import java.util.ArrayList;

import jdraw.framework.FigureHandle; 


import jdraw.pollakg.figures.handles.*;
import jdraw.pollakg.figures.AbstractRectangularFigure;
/**
 * @author Georg pollak
 */

public abstract class AbstractRectangularFigure extends AbstractFigure {

    protected final RectangularShape shape;
    protected List<FigureHandle> handles = new ArrayList<>(8);

    AbstractRectangularFigure(RectangularShape shape) {
        this.shape = shape;
        handles.add(new Handle(new NWHandle(this)));
        handles.add(new Handle(new NEHandle(this)));
        handles.add(new Handle(new SWHandle(this)));
        handles.add(new Handle(new SEHandle(this)));
        handles.add(new Handle(new NHandle(this)));
        handles.add(new Handle(new EHandle(this)));
        handles.add(new Handle(new SHandle(this)));
        handles.add(new Handle(new WHandle(this)));
    }
    
    AbstractRectangularFigure(AbstractRectangularFigure f) {
        this((RectangularShape) f.shape.clone());
    }

    @Override
    public abstract void draw(Graphics g);

    @Override
    public void setBounds(Point origin, Point corner) {
        Rectangle original = shape.getBounds();

        shape.setFrameFromDiagonal(origin, corner);

        if (!original.equals(shape.getBounds())) {
            notifyAllListeners();
        }
    }

    @Override
    public void move(int dx, int dy) {
        if (dx != 0 || dy != 0) {
            ((RectangularShape) shape).setFrame(
                shape.getX()+dx,
                shape.getY()+dy,
                shape.getWidth(),
                shape.getHeight()
            );
            notifyAllListeners();
        }
    }

    @Override
    public boolean contains(int x, int y) {
        return shape.contains(x, y);
    }

    @Override
    public Rectangle getBounds() {
        return shape.getBounds();
    }

    @Override
    public List<FigureHandle> getHandles() {
        return handles;
    }

    public void swapVertically() {
    	// Important we can not do
    	// FigureHandle H = handles.get(1);
    	// And then HandleState H = H.getState()
    	// As the interface FigureHandle does not 
    	// imlement getState
    	Handle NW = (Handle) handles.get(0);
        Handle NE = (Handle) handles.get(1);
        Handle SW = (Handle) handles.get(2);
        Handle SE = (Handle) handles.get(3);
        Handle E  = (Handle) handles.get(5);
        Handle W  = (Handle) handles.get(7);
		HandleState NWstate = NW.getState();
        HandleState NEstate = NE.getState();
        HandleState SWstate = SW.getState();
        HandleState SEstate = SE.getState();
        HandleState Estate  =  E.getState();
        HandleState Wstate  =  W.getState();
        NW.setState(NEstate);
        NE.setState(NWstate);
        SW.setState(SEstate);
        SE.setState(SWstate);
        E.setState(Wstate);
        W.setState(Estate);
    }

    public void swapHorizontally() {
    	Handle NW = (Handle) handles.get(0);
        Handle NE = (Handle) handles.get(1);
        Handle SW = (Handle) handles.get(2);
        Handle SE = (Handle) handles.get(3);
        Handle N  = (Handle) handles.get(4);
        Handle S  = (Handle) handles.get(6);
        HandleState NWstate = NW.getState();
        HandleState NEstate = NE.getState();
        HandleState SWstate = SW.getState();
        HandleState SEstate = SE.getState();
        HandleState Nstate  =  N.getState();
        HandleState Sstate  =  S.getState();
        NW.setState(SWstate);
        NE.setState(SEstate);
        SW.setState(NWstate);
        SE.setState(NEstate);
        N.setState(Sstate);
        S.setState(Nstate);
    }
}
