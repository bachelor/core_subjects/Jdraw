package jdraw.pollakg.figures;
	
import java.awt.geom.Line2D;	
import java.awt.geom.Point2D;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import jdraw.framework.Figure;
import jdraw.framework.FigureEvent;
import jdraw.framework.FigureHandle;
import jdraw.framework.FigureListener;

import jdraw.pollakg.figures.handles.Handle;
import jdraw.pollakg.figures.handles.LineStartHandle;
import jdraw.pollakg.figures.Line;
import jdraw.pollakg.figures.handles.LineEndHandle;
	
/**
 * Represents lines in JDraw.
 * 
 * @author Georg Pollak
 *
 */
public class Line extends AbstractFigure {
	/**
	 * Use the java.awt.geom.Line2D in order to save/reuse code.
	 */
	private final Line2D.Float line;
	private List<FigureHandle> handles = new ArrayList<FigureHandle>(2);
	

  private static final int MOVING_TOL = 5;
    /**
     * Create a new line of the given dimension.
     * @param x1 the first x-coordinate of the line
     * @param y1 the first y-coordinate of the line
     * @param x2 the second x-coordinate of the line
     * @param y2 the second y-coordinate of the line
     */
    public Line(int x1, int y1, int x2, int y2) {
        line = new Line2D.Float(x1, y1, x2, y2);
    }

    Line(Point p) {
        this(p.x, p.y, p.x, p.y);
        handles.add(new Handle(new LineStartHandle(this)));
        handles.add(new Handle(new LineEndHandle(this)));
    }
    
    Line(Line l) {
        this((int) l.line.getX1(), (int) l.line.getY1(), (int) l.line.getX2(), (int) l.line.getY2());
    }

    public Point getP1() {
        Point2D p1 = line.getP1();
        return new Point((int) p1.getX(), (int) p1.getY());
    }

    public Point getP2() {
        Point2D p2 = line.getP2();
        return new Point((int) p2.getX(), (int) p2.getY());
    }
	/**
	 * Draw the line to the given graphics context.
	 * @param g the graphics context to use for drawing.
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawLine((int) line.x1, (int) line.y1, (int) line.x2, (int) line.y2);
	}
	
	@Override
	public void setBounds(Point origin, Point corner) {
		line.setLine(origin, corner);
        notifyAllListeners();
	}

	@Override
	public void move(int dx, int dy) {
        if (dx != 0 && dy != 0) {
    		line.setLine(line.x1 + dx, line.y1 + dy, line.x2 + dx, line.y2 + dy);
            notifyAllListeners();
        }
	}

	@Override
	public boolean contains(int x, int y) {
    return line.ptSegDist(x, y) <= MOVING_TOL;
	}

	@Override
	public Rectangle getBounds() {
		return line.getBounds();
	}

    @Override
    public List<FigureHandle> getHandles() {
        return handles;
    }

    @Override
    public Line clone() {
            return new Line(this);
    }
}
