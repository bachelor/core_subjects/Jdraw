package jdraw.pollakg.figures.handles;

import jdraw.framework.DrawView;	
import jdraw.framework.Figure;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import jdraw.pollakg.figures.AbstractRectangularFigure;
import java.awt.event.MouseEvent;

/**
 * @author Georg Pollak
 */

public class SWHandle extends HandleState {

    public SWHandle(Figure owner) {
        super(owner);
    }

    @Override
    public Point getLocation() {
        Rectangle r = getOwner().getBounds();
        return new Point(r.x, r.y+r.height);
    }

    @Override
    public Cursor getCursor() {
        return Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
    }

    @Override
    public void dragInteraction(int x, int y, MouseEvent e, DrawView v) {
        Rectangle r = getOwner().getBounds();
        getOwner().setBounds(
                new Point(x, r.y),
                new Point(r.x+r.width, y)
        );

	        if (x > r.x+r.width) {
	            ((AbstractRectangularFigure) getOwner()).swapVertically();
	        } else if (y < r.y) {
	            ((AbstractRectangularFigure) getOwner()).swapHorizontally();
	        }
    }
}
