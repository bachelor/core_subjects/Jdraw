package jdraw.pollakg.figures.handles;

import java.awt.Color;		
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import jdraw.framework.DrawView;
import jdraw.framework.Figure;
import jdraw.framework.FigureHandle;

public abstract class HandleState implements FigureHandle {
	
    private final Figure owner;
    private static final int SIZE = 10;
    
    protected HandleState(Figure owner) {
        this.owner = owner;
    }
    
	@Override
	public Figure getOwner() {
        return owner;
	}

	@Override
	public void draw(Graphics g) {
		 Point loc = getLocation();
	     g.setColor(Color.WHITE);
	     g.fillRect(loc.x - SIZE/2, loc.y - SIZE/2, SIZE, SIZE);
	     g.setColor(Color.BLACK);
	     g.drawRect(loc.x - SIZE/2, loc.y - SIZE/2, SIZE, SIZE);
	}

	@Override
	public boolean contains(int x, int y) {
        Point loc = getLocation();
        Rectangle bounds = new Rectangle(loc.x - SIZE/2, loc.y - SIZE/2, SIZE, SIZE);
        return bounds.contains(x, y);
	}

	// Be carefull could do IllegalStateException but then each concrete handle
	// needs to overwrite them all as all of these methods get called during interaction
	// => State exception if not overwritten
	@Override
	public void startInteraction(int x, int y, MouseEvent e, DrawView v){
		//throw new IllegalStateException("Method Not Suppoted");
	}
	@Override
	public void dragInteraction(int x, int y, MouseEvent e, DrawView v){
		//throw new IllegalStateException("Method Not Suppoted");
	}
	@Override
	public void stopInteraction(int x, int y, MouseEvent e, DrawView v){
		//throw new IllegalStateException("Method Not Suppoted");

	}
	
}
