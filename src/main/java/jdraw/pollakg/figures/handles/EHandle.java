package jdraw.pollakg.figures.handles;

import jdraw.framework.DrawView;		
import jdraw.framework.Figure;
import jdraw.pollakg.figures.AbstractRectangularFigure;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

/**
 * @author Georg Pollak
 */

public class EHandle extends HandleState {

    public EHandle(Figure owner) {
        super(owner);
    }

    @Override
    public Point getLocation() {
        Rectangle r = getOwner().getBounds();
        return new Point(r.x+r.width, r.y+r.height/2);
    }

    @Override
    public Cursor getCursor() {
        return Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
    }

    @Override
    public void dragInteraction(int x, int y, MouseEvent e, DrawView v) {
        Rectangle r = getOwner().getBounds();
        getOwner().setBounds(
                new Point(r.x, r.y),
                new Point(x, r.y+r.height)
         );

        if (x < r.x) {
        	((AbstractRectangularFigure) getOwner()).swapVertically();
        }
    }
}
