package jdraw.pollakg.figures.handles;

import jdraw.framework.DrawView;		
import jdraw.framework.Figure;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import jdraw.pollakg.figures.AbstractRectangularFigure;
import java.awt.event.MouseEvent;

/**
 * @author Georg Pollak
 */

public class NHandle extends HandleState {

    public NHandle(Figure owner) {
        super(owner);
    }

    @Override
    public Point getLocation() {
        Rectangle r = getOwner().getBounds();
        return new Point(r.x+r.width/2, r.y);
    }

    @Override
    public Cursor getCursor() {
        return Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);
    }

    @Override
    public void dragInteraction(int x, int y, MouseEvent e, DrawView v) {
        Rectangle r = getOwner().getBounds();
        getOwner().setBounds(
                new Point(r.x, y),
                new Point(r.x+r.width, r.y+r.height)
        );

        if (y > r.y+r.height) {
            ((AbstractRectangularFigure) getOwner()).swapHorizontally();
        }
    }
}
