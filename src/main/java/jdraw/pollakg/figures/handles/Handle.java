package jdraw.pollakg.figures.handles;

import jdraw.framework.DrawView;		
import jdraw.framework.Figure;
import jdraw.framework.FigureHandle;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;

/**
 * @author Georg Pollak
 * Context Class
 */

public class Handle implements FigureHandle {

    private HandleState state;

    public Handle(HandleState state) {
        this.state = state;
    }

    public void setState(HandleState state) {
        this.state = state;
    }

    public HandleState getState() {
        return state;
    }

    public Figure getOwner() {
        return state.getOwner();
    }

    public Point getLocation() {
        return state.getLocation();
    }

    public void draw(Graphics g) {
	    state.draw(g);
    }

    public Cursor getCursor() {
        return state.getCursor();
    }

    public boolean contains(int x, int y) {
        return state.contains(x, y);
    }

    public void startInteraction(int x, int y, MouseEvent e, DrawView v) {
        state.startInteraction(x, y, e, v);
    }
    
    public void dragInteraction(int x, int y, MouseEvent e, DrawView v) {
        state.dragInteraction(x, y, e, v);
    }

    public void stopInteraction(int x, int y, MouseEvent e, DrawView v) {
        state.stopInteraction(x, y, e, v);
    }

}