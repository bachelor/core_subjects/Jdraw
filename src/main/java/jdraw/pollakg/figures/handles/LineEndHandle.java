package jdraw.pollakg.figures.handles;

import jdraw.pollakg.figures.Line;

import jdraw.framework.DrawView;
import jdraw.framework.Figure;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;

/**
 * @author Georg Pollak
 */

public class LineEndHandle extends HandleState {

    private Point fixed;

    public LineEndHandle(Figure owner) {
        super(owner);
    }

    @Override
    public Point getLocation() {
        return ((Line) getOwner()).getP2();
    }

    @Override
    public Cursor getCursor() {
        return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
    }

    @Override
    public void startInteraction(int x, int y, MouseEvent e, DrawView v) {
        fixed =  ((Line) getOwner()).getP1();
    }

    @Override
    public void dragInteraction(int x, int y, MouseEvent e, DrawView v) {
        getOwner().setBounds(fixed, new Point(x, y));
    }

}
