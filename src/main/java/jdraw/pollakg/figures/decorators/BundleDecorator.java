package jdraw.pollakg.figures.decorators;

import java.util.List;

import java.awt.Point;

import jdraw.framework.Figure;
import jdraw.framework.FigureHandle;

/**
 * @author Georg Pollak
 */

public class BundleDecorator extends AbstractDecorator {

        public BundleDecorator(Figure f) {
                super(f);
        }

        @Override
        public void move(int dx, int dy) {
                // ignore
        }

        @Override
        public void setBounds(Point origin, Point corner) {
                // ignore
        }

        @Override
        public List<FigureHandle> getHandles() {
                return null;
        }
}
