package jdraw.pollakg.figures.decorators;

import java.awt.Rectangle;	
import java.awt.Graphics;
import java.awt.Color;

import jdraw.framework.Figure;

/**
 * @author Georg Pollak
 */

public class BorderDecorator extends AbstractDecorator {

        private static final int BORDER_DIST = 5;
        
        public BorderDecorator(Figure f) {
                super(f);
        }

        @Override
        public void draw(Graphics g) {
                Rectangle bounds = inner.getBounds();

                int x = (int) bounds.getX() - BORDER_DIST;
                int y = (int) bounds.getY() - BORDER_DIST;
                int w = (int) bounds.getWidth()  + 2*BORDER_DIST;
                int h = (int) bounds.getHeight() + 2*BORDER_DIST;

                g.setColor(Color.WHITE);
                g.drawLine(x, y, x+w, y);
                g.drawLine(x, y, x, y+h);
                g.setColor(Color.DARK_GRAY);
                g.drawLine(x+w, y, x+w, y+h);
                g.drawLine(x, y+h, x+w, y+h);

                inner.draw(g);
        }

        @Override
        public boolean contains(int x, int y) {
                return getBounds().contains(x, y);
        }

        @Override
        public Rectangle getBounds() {
                Rectangle bounds = inner.getBounds();
                bounds.grow(BORDER_DIST, BORDER_DIST);
                return bounds;
        }

}
