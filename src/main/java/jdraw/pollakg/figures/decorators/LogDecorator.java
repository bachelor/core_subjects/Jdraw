package jdraw.pollakg.figures.decorators;

import java.util.List;	

import java.awt.Rectangle;
import java.awt.Point;
import java.awt.Graphics;

import jdraw.framework.Figure;
import jdraw.framework.FigureEvent;
import jdraw.framework.FigureHandle;
import jdraw.framework.FigureListener;

/**
 * @author Georg Pollak
 */

public class LogDecorator extends AbstractDecorator {

        public LogDecorator(Figure f) {
                super(f);
        }

        @Override
        public void draw(Graphics g) {
                System.out.println("Calling draw() on Figure " + inner);
                inner.draw(g);
        }

        @Override
        public void move(int dx, int dy) {
                System.out.println("Calling move() on Figure " + inner);
                inner.move(dx, dy);
        }

        @Override
        public boolean contains(int x, int y) {
                System.out.println("Calling contains() on Figure " + inner);
                return inner.contains(x, y);
        }

        @Override
        public void setBounds(Point origin, Point corner) {
                System.out.println("Calling setBounds() on Figure " + inner);
                inner.setBounds(origin, corner);
        }

        @Override
        public Rectangle getBounds() {
                System.out.println("Calling getBounds() on Figure " + inner);
                return inner.getBounds();
        }

        @Override
        public List<FigureHandle> getHandles() {
                System.out.println("Calling getHandles() on Figure " + inner);
                return inner.getHandles();
        }


        @Override
        public void addFigureListener(FigureListener listener) {
                System.out.println("Calling addFigureListener() on Figure " + inner);
                inner.addFigureListener(listener);
        }

        @Override
        public void removeFigureListener(FigureListener listener) {
                System.out.println("Calling removeFigureListener() on Figure " + inner);
                inner.removeFigureListener(listener);
        }

        @Override
        public Figure clone() {
                System.out.println("Calling clone() on Figure " + inner);
                return (Figure) inner.clone();
        }
}
