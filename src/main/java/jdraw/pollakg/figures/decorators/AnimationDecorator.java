package jdraw.pollakg.figures.decorators;

import java.awt.Rectangle;

import jdraw.framework.Figure;

/**
 * @author Georg Pollak
 */

public class AnimationDecorator extends AbstractDecorator {

        private AnimationThread thread;

        public AnimationDecorator(Figure f) {
                super(f);
                thread = new AnimationThread(this);
                thread.start();
        }

        @Override
        public Figure getInner() {
                thread.stopThread();
                return inner;
        }

        private static class AnimationThread extends Thread {
                private Figure f;
                private volatile boolean stop = false;
                private int dx = 1, dy = 1;

                AnimationThread(Figure f) {
                        this.f = f;
                        this.setDaemon(true);
                }

                public void stopThread() {
                        stop = true;
                }

                @Override
                public void run() {
                        while (!stop) {
                                Rectangle r = f.getBounds();
                                if (r.x + r.width > 500 && dx > 0) { dx = -dx; }
                                if (r.x <= 0 && dx < 0) { dx = -dx; }
                                if (r.y + r.height > 500 && dy > 0) { dy = -dy; }
                                if (r.y <= 0 && dy < 0) { dy = -dy; }
                                f.move(dx, dy);
                                try {
                                        Thread.sleep(50);
                                } catch (InterruptedException e) {
                                        stop = true;
                                }
                        }
                }
        }

}
