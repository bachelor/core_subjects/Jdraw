package jdraw.pollakg.figures.decorators;

import java.util.List;	
import java.util.ArrayList;

import java.awt.Rectangle;
import java.awt.Point;
import java.awt.Graphics;
import java.awt.Cursor;
import java.awt.event.MouseEvent;

import jdraw.framework.DrawView;
import jdraw.framework.Figure;
import jdraw.pollakg.figures.AbstractFigure;
import jdraw.framework.FigureEvent;
import jdraw.framework.FigureHandle;
import jdraw.framework.FigureListener;


/**
 * @author Georg Pollak
 */

@SuppressWarnings("serial")
public class AbstractDecorator extends 
			 AbstractFigure implements
			 FigureListener {

        protected Figure inner;

        AbstractDecorator(Figure f) {
            this.inner = f;
            // Decorator listens to figure changes
        	inner.addFigureListener(this);
        }
        
        public Figure getInner() {
                return inner;
        }

        @Override
        public void draw(Graphics g) {
                inner.draw(g);
        }

        @Override
        public void move(int dx, int dy) {
                inner.move(dx, dy);
        }

        @Override
        public boolean contains(int x, int y) {
                return inner.contains(x, y);
        }

        @Override
        public void setBounds(Point origin, Point corner) {
                inner.setBounds(origin, corner);
        }

        @Override
        public Rectangle getBounds() {
                return inner.getBounds();
        }
        
        @Override
        public void figureChanged(FigureEvent e) { 
        /* 
         * We implement the figure listener interface (add to solution)
           => need to implement figureChanged
           Note: Abstract figure does not implement FigureListerner interface
           as it is not an listener 
           But it provides a method to notify all listeners upon a change
           which we can use here to notify all Figures that listen to the 
           decorator
           protected void notifyAllListeners() {
			    for (FigureListener l : listeners) {
			    	l.figureChanged(new FigureEvent(this));
    			}
			}
         */
        	notifyAllListeners();
        }

        private final class DecoratorHandle implements FigureHandle {
        		// Basically another decorator for the handles
        		// Only changes the owner method!
        	    // Note: swap is an method of get.Owner()
        		// And thus of figure decorator
                private final FigureHandle inner;

                public DecoratorHandle(FigureHandle handle) {
                    this.inner = handle;    
                }

                @Override
                public Figure getOwner() {
                	// Want to return this of superclass and not of handle
                    return AbstractDecorator.this;	
                }

                @Override
                public Cursor getCursor() {
                        return inner.getCursor();
                }

                @Override
                public Point getLocation() {
                        return inner.getLocation();
                }

                @Override
                public void draw(Graphics g) {
                        inner.draw(g);
                }

                @Override
                public boolean contains(int x, int y) {
                        return inner.contains(x, y);
                }

                @Override
                public void dragInteraction(int x, int y, MouseEvent e, DrawView v) {
                        inner.dragInteraction(x, y, e, v);
                }

                @Override
                public void startInteraction(int x, int y, MouseEvent e, DrawView v) {
                        inner.startInteraction(x, y, e, v);
                }

                @Override
                public void stopInteraction(int x, int y, MouseEvent e, DrawView v) {
                        inner.stopInteraction(x, y, e, v);
                }
        }

        @Override
        public List<FigureHandle> getHandles() {
                List<FigureHandle> handles = new ArrayList<FigureHandle>(inner.getHandles().size());
                for (FigureHandle h: inner.getHandles()) {
                        handles.add(new DecoratorHandle(h));
                }
                return handles;
        }

        @Override
        public void addFigureListener(FigureListener listener) {
                inner.addFigureListener(listener);
        }

        @Override
        public void removeFigureListener(FigureListener listener) {
                inner.removeFigureListener(listener);
        }

        @Override
        public Figure clone() {
            AbstractDecorator copy = (AbstractDecorator) super.clone();
            copy.inner = (Figure) inner.clone();
            // Add cloned decorator as listener to 
            // deeply cloned figure
            copy.inner.addFigureListener(copy);
            return copy;
        }

}
