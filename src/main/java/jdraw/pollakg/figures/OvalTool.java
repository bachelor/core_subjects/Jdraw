package jdraw.pollakg.figures;

import java.awt.Point;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import jdraw.framework.DrawContext;
import jdraw.framework.DrawView;

/**
 * This tool defines a mode for drawing ellipses.
 *
 * @see jdraw.framework.Figure
 *
 * @author Georg Pollak
 */
public class OvalTool extends AbstractFigureTool {

	/**
	 * Create a new ellipse tool for the given context.
	 * @param context a context to use this tool in.
	 */
	public OvalTool(DrawContext context) {
        super(context);
    }
	
	@Override
	public Icon getIcon() {
		return new ImageIcon(getClass().getResource(IMAGES + "oval.png"));
	}

	@Override
	public String getName() {
		return "Ellipse";
	}

    @Override
    protected AbstractFigure createFigure(Point p) {
        return new Oval(p);
    }

}
