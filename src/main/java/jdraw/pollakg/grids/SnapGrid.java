package jdraw.pollakg.grids;

import java.awt.Point;	
import java.util.ArrayList;

import jdraw.framework.DrawGrid;
import jdraw.framework.DrawContext;
import jdraw.framework.Figure;
import jdraw.framework.FigureHandle;

/**
 * @author Georg Pollak
 */

public class SnapGrid extends AbstractGridConstrainer {

        private final static int SNAP_DIST = 25;

        private Figure current;

        public SnapGrid(DrawContext c) {
                super(c, "Snap Grid", 1, 1);
                this.context.getModel().addModelChangeListener(e -> current = e.getFigure());
        }

        @Override
        public Point constrainPoint(Point p) {
                ArrayList<Point> closePoints = new ArrayList<Point>();

                this.context.getModel().getFigures().forEach(f -> {
                        if (!this.context.getView().getSelection().contains(f)
                                        && f != current) {
                                f.getHandles().forEach(h -> {
                                        if (p.distance(h.getLocation()) <= SNAP_DIST) {
                                                closePoints.add(h.getLocation());
                                        }
                                });
                                        }
                });
                closePoints.add(p);

                return closePoints.get(0);
        }
}
