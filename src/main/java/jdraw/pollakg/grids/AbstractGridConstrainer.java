package jdraw.pollakg.grids;

import java.awt.Point;		

import jdraw.pollakg.grids.PointConstrainer;
import jdraw.framework.DrawContext;

public class AbstractGridConstrainer implements PointConstrainer {

    protected final DrawContext context;
    private final int stepX, stepY;
    private final String name;
    public AbstractGridConstrainer(DrawContext c, String n, int sx, int sy) {
        this.context = c;
        this.name = n;
        this.stepX = Math.max(1, sx);
        this.stepY = Math.max(1, sy);
    }

    @Override
    public Point constrainPoint(Point p) {
        int newX = stepX * (Math.round(p.x / stepX));
        int newY = stepY * (Math.round(p.y / stepY));
        return new Point(newX, newY);
    }

    @Override public int getStepX(boolean right) { return stepX; }
    @Override public int getStepY(boolean down) { return stepY; }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void activate() {
        this.context.showStatusText("Activated " + name);
    }

    @Override
    public void deactivate() {
        this.context.showStatusText("Deactivated " + name);
    }

    @Override
    public void mouseDown() { }
    @Override
    public void mouseUp() { }

}