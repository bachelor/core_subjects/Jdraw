package jdraw.pollakg.grids;

import jdraw.framework.DrawGrid;

public interface PointConstrainer extends DrawGrid {
	public String getName();
}
