/*
 * Copyright (c) 2017 Fachhochschule Nordwestschweiz (FHNW)
 * All Rights Reserved. 
 */

package jdraw.pollakg;

import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

import jdraw.std.EmptyDrawCommandHandler;

import jdraw.framework.DrawCommandHandler;
import jdraw.framework.DrawModel;
// Tells DrawModelListener what changed
import jdraw.framework.DrawModelEvent;
import jdraw.framework.DrawModelListener;
import jdraw.framework.Figure;
import jdraw.framework.FigureEvent;
// Figure listener requires implementation of figureChanged(FigureEvent) method
// to notify figures Listeners that the figure changed
import jdraw.framework.FigureListener;
// Public enum defined in DrawModelEvent
import jdraw.framework.DrawModelEvent.Type;


/**
 * Provide a standard behavior for the drawing model. This class initially does not implement the methods
 * in a proper way.
 * It is part of the course assignments to do so.
 * @author Georg Pollak
 *
 */
public class StdDrawModel implements DrawModel, FigureListener {
	
	
	// Draw Model contains implementations of figures
	private CopyOnWriteArrayList<Figure> figures = new CopyOnWriteArrayList<>();
	// Listens if the model changes e.g. figure added, removed,...
	// Thus in those methods we need to the respective listener
	private CopyOnWriteArrayList<DrawModelListener> listeners = new CopyOnWriteArrayList<>();
	
	private void notifyAllListeners(DrawModelEvent e) {
		// inform all DrawModelListenrs that a figure changed added, removed,...
		for(DrawModelListener l : listeners) {
			l.modelChanged(e);
		}
	}
	
	@Override
	public void addFigure(Figure f) {
		// DONE to be implemented
		// Only use unique figures (testAddFigure4 test)
		// and listeners (testAddFigure5)
		if(f == null || figures.contains(f)) return;
		figures.add(f);
		// Need to register figure listener with the method addFigureListner
		// Note StdDrawMolel is the Concrete Figure listener
		f.addFigureListener(this);		
		// Need to inform DrawModelListeners that event has occurred/update
		// Need to specify which event has occurred 
		// For DrawModelEvent see jdraw.framework/DrawModelEvent
		// DrawModelEvent(DrawModel source, Figure figure, Type type)
		// Could also use DrawModelEvent.Type.FIGURE_ADDED
		notifyAllListeners(new DrawModelEvent(this, f, Type.FIGURE_ADDED));
	}

	@Override
	public Iterable<Figure> getFigures() {
      // Done to be implemented  
      // we do not want that anyone can modify our figures
      return Collections.unmodifiableList(figures);
	}

    @Override
    public void removeFigure(Figure f) {
        // DONE to be implemented
        // If we do not have a figures do nothing (testRemoveFigure1)
        if(!figures.contains(f)) return;
        figures.remove(f);
        f.removeFigureListener(this);		
        notifyAllListeners(new DrawModelEvent(this, f, Type.FIGURE_REMOVED));
    }
	
    // DONE: Draw model listeners get informed as well on FigureChagned events
    @Override
    public void figureChanged(FigureEvent e) {
        // Notify listeners that a figure changed e.g. form or position
        notifyAllListeners( new DrawModelEvent(this, e.getFigure(), Type.FIGURE_CHANGED) );
    }

    @Override
    public void addModelChangeListener(DrawModelListener listener) {
        // DONE to be implemented 
        if (listener != null && !listeners.contains(listener))
            listeners.add(listener);
        }

    @Override
    public void removeModelChangeListener(DrawModelListener listener) {
        // DONE to be implemented  

        if (listener != null)
            listeners.remove(listener);
        }

        /** The draw command handler. Initialized here with a dummy implementation. */
        // TODO initialize with your implementation of the undo/redo-assignment.
    private DrawCommandHandler handler = new EmptyDrawCommandHandler();

    /**
     * Retrieve the draw command handler in use.
     * @return the draw command handler.
     */
    @Override
    public DrawCommandHandler getDrawCommandHandler() {
        return handler;
    }

    @Override
    public void setFigureIndex(Figure f, int index) {
        // DONE to be implemented  
        // Changes index/place of figure in list
        // java.util.LinkedList.add(int Index, Object o)
        // java.util.LinkedList.remove(Object o):
        // removes first occurrence of the object o if it exists
        // Needed to pass testSetFigureIndex2
        if(!figures.contains(f)) throw new IllegalArgumentException();
        // Needed to pass testSetFigureIndex3
        if(index < 0 || index >= figures.size()) throw new IndexOutOfBoundsException();
        figures.remove(f);
        figures.add(index, f);
        // UNDERDSTAND: why we need to do this or if we can omit it
        // Explanation: need to notify what changed
        figureChanged(new FigureEvent(f));
        // Necessary to pass testSetFigureIndex1
        notifyAllListeners(new DrawModelEvent(this, f, Type.DRAWING_CHANGED));
    }

    @Override
    public void removeAllFigures() {
        // TODO to be implemented  
        // NOTE: Junit test testRemvoveAllFigures2 requires us to remove the 
        // figure listeners before the figures explicitly 
        // hence it is not enough to remove the listeners using the
        // remove figures method
        for(Figure f : figures) {
            f.removeFigureListener(this);
        }
        // java.util.LinkedList.clear() rmoves all elements from list
        // could also first only loop and remove figure listeners and afterwards clear whole 
        // list explicitly this is what we have to do due to unit test
        /*		
              for(Figure f : figures) {
              f.removeFigureListeners(this);
              }
              figures.clear();
        */
        // Junit test testRemoveAllfigures2: need to remove all listeners
        figures.clear();
        // Junit test testRemoveAllfigures1
        notifyAllListeners(new DrawModelEvent(this, null, DrawModelEvent.Type.DRAWING_CLEARED));	
    }

}
