package jdraw.pollakg.actions;

import jdraw.pollakg.figures.decorators.AnimationDecorator;

import java.util.List;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import jdraw.framework.DrawContext;
import jdraw.framework.Figure;
import jdraw.framework.FigureListener;

/**
 * @author Georg Pollak
 */

public class AddAnimationDecoratorAction extends AbstractAction implements MenuListener {

        public AddAnimationDecoratorAction(DrawContext context, JMenu menu) {
                super(context);
                putValue(Action.NAME, "Add Animation");
                menu.addMenuListener(this);
        }

        @Override
        public void menuSelected(MenuEvent ignore) {
                setEnabled(context.getView().getSelection().size() >= 1);
        }

        @Override
        public void actionPerformed(ActionEvent ignore) {
                for (Figure f: context.getView().getSelection()) {
                        Figure decF = new AnimationDecorator(f);
                        f.addFigureListener((FigureListener) decF);

                        context.getModel().removeFigure(f);
                        context.getView().removeFromSelection(f);

                        context.getModel().addFigure(decF);
                        context.getView().addToSelection(decF);
                }
        }
}
