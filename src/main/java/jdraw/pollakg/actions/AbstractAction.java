package jdraw.pollakg.actions;

import java.util.List;		

import java.awt.event.ActionEvent;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import jdraw.framework.DrawContext;

/**
 * @author Georg Pollak
 */

public class AbstractAction extends javax.swing.AbstractAction implements MenuListener {

        protected static final String IMAGES = "/images/";
        protected final DrawContext context;

        public AbstractAction(DrawContext context) {
                this.context = context;
        }

        public void menuSelected(MenuEvent ignore) {
                setEnabled(true);
        }

        public void menuDeselected(MenuEvent ignore) {
                setEnabled(true);
        }

        public void menuCanceled(MenuEvent ignore) {
                setEnabled(true);
        }

        public void actionPerformed(ActionEvent ignore) { }
}
