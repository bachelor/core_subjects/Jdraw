package jdraw.pollakg.actions;

import jdraw.pollakg.std.SimpleClipboard;

import java.util.List;
import java.util.LinkedList;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import jdraw.framework.DrawContext;
import jdraw.framework.Figure;

/**
 * @author Georg Pollak
 */

public class PasteAction extends AbstractAction implements MenuListener {

        public PasteAction(DrawContext context, JMenu menu) {
                super(context);
                putValue(Action.SMALL_ICON, new ImageIcon(getClass().getResource(IMAGES + "paste.png")));
                putValue(Action.SHORT_DESCRIPTION, "Pastes all figures from the clipboard");
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
                putValue(Action.NAME, "Paste");
                menu.addMenuListener(this);
        }

        @Override
        public void menuSelected(MenuEvent ignore) {
                setEnabled(SimpleClipboard.get().size() >= 1);
        }

        @Override
        public void actionPerformed(ActionEvent ignore) {
                context.getView().clearSelection();
                List<Figure> pastedClones = new LinkedList<Figure>();
                for (Figure f: SimpleClipboard.get()) {
                        Figure c = f.clone();
                        c.move(25, 25);
                        context.getModel().addFigure(c);
                        context.getView().addToSelection(c);
                        pastedClones.add(c);
                }
        }
}
