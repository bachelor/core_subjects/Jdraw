package jdraw.pollakg.actions;

import jdraw.pollakg.figures.GroupFigure;

import java.util.List;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import jdraw.framework.DrawContext;
import jdraw.framework.Figure;

/**
 * @author Georg Pollak
 */

public class GroupAction extends AbstractAction implements MenuListener {

        public GroupAction(DrawContext context, JMenu menu) {
                super(context);
                putValue(Action.SMALL_ICON, new ImageIcon(getClass().getResource(IMAGES + "group.png")));
                putValue(Action.SHORT_DESCRIPTION, "Groups all selected figures");
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control G"));
                putValue(Action.NAME, "Group");
                menu.addMenuListener(this);
        }

        @Override
        public void menuSelected(MenuEvent ignore) {
                setEnabled(context.getView().getSelection().size() > 1);
        }

        @Override
        public void actionPerformed(ActionEvent ignore) {
                List<Figure> sel = context.getView().getSelection();
                if (sel != null && sel.size() >= 2) {
                    GroupFigure g = new GroupFigure(sel);
                    for (Figure f: sel) {
                            context.getModel().removeFigure(f);
                            context.getView().removeFromSelection(f);
                    }
                    context.getModel().addFigure(g);
                    context.getView().addToSelection(g);
                }
        }
}
