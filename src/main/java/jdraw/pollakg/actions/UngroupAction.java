package jdraw.pollakg.actions;

import jdraw.pollakg.figures.GroupFigure;

import java.util.List;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import jdraw.framework.DrawContext;
import jdraw.framework.Figure;

/**
 * @author Georg Pollak
 */

public class UngroupAction extends AbstractAction implements MenuListener {

        public UngroupAction(DrawContext context, JMenu menu) {
                super(context);
                putValue(Action.SMALL_ICON, new ImageIcon(getClass().getResource(IMAGES + "ungroup.png")));
                putValue(Action.SHORT_DESCRIPTION, "Splits selected groups into its parts");
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control U"));
                putValue(Action.NAME, "Ungroup");
                menu.addMenuListener(this);
        }

        @Override
        public void menuSelected(MenuEvent ignore) {
                boolean isGroupSelected = false;
                for (Figure f: context.getView().getSelection()) {
                        if (f instanceof GroupFigure) {
                                isGroupSelected = true;
                                break;
                        }
                }
                setEnabled(isGroupSelected);
        }

        @Override
        public void actionPerformed(ActionEvent ignore) {
            List<Figure> sel = context.getView().getSelection();
            if (sel != null) {
                for (Figure f: sel) {
                    if (f instanceof GroupFigure) {
                        GroupFigure g = (GroupFigure) f;
                        context.getModel().removeFigure(f);
                        context.getView().removeFromSelection(f);
                        for (Figure p: g.getFigureParts()) {
                                context.getModel().addFigure(p);
                                context.getView().addToSelection(p);
                        }
                    }
                }
            }
        }
}
