package jdraw.pollakg.actions;

import jdraw.pollakg.std.SimpleClipboard;

import java.util.List;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import jdraw.framework.DrawContext;
import jdraw.framework.Figure;

/**
 * @author Georg Pollak
 */

public class CopyAction extends AbstractAction implements MenuListener {

        public CopyAction(DrawContext context, JMenu menu) {
                super(context);
                putValue(Action.SMALL_ICON, new ImageIcon(getClass().getResource(IMAGES + "copy.png")));
                putValue(Action.SHORT_DESCRIPTION, "Copies all selected figures");
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
                putValue(Action.NAME, "Copy");
                menu.addMenuListener(this);
        }

        @Override
        public void menuSelected(MenuEvent ignore) {
                setEnabled(context.getView().getSelection().size() >= 1);
        }

        @Override
        public void actionPerformed(ActionEvent ignore) {
             
                SimpleClipboard.clear();
                for (Figure f: context.getView().getSelection()) {
                        SimpleClipboard.add(f.clone());
                }
        }
}
