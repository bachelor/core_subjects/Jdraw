package jdraw.pollakg.actions;

import jdraw.pollakg.figures.decorators.LogDecorator;

import java.util.List;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import jdraw.framework.DrawContext;
import jdraw.framework.Figure;
import jdraw.framework.FigureListener;

/**
 * @author Georg Pollak
 */

public class RemoveLogDecoratorAction extends AbstractAction implements MenuListener {

        public RemoveLogDecoratorAction(DrawContext context, JMenu menu) {
                super(context);
                putValue(Action.NAME, "Remove Logger");
                menu.addMenuListener(this);
        }

        @Override
        public void menuSelected(MenuEvent ignore) {
                boolean isDecoratorSelected = false;
                for (Figure f: context.getView().getSelection()) {
                        if (f instanceof LogDecorator) {
                                isDecoratorSelected = true;
                                break;
                        }
                }
                setEnabled(isDecoratorSelected);
        }

        @Override
        public void actionPerformed(ActionEvent ignore) {
                for (Figure f: context.getView().getSelection()) {
                        Figure undecF = ((LogDecorator) f).getInner();
                        undecF.removeFigureListener((FigureListener) f);

                        context.getModel().removeFigure(f);
                        context.getView().removeFromSelection(f);

                        context.getModel().addFigure(undecF);
                        context.getView().addToSelection(undecF);
                }
        }
}
