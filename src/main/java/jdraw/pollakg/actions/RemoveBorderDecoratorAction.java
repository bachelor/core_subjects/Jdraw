package jdraw.pollakg.actions;

import jdraw.pollakg.figures.decorators.BorderDecorator;

import java.util.List;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import jdraw.framework.DrawContext;
import jdraw.framework.Figure;
import jdraw.framework.FigureListener;

/**
 * @author Georg Pollak
 */

public class RemoveBorderDecoratorAction extends AbstractAction implements MenuListener {

        public RemoveBorderDecoratorAction(DrawContext context, JMenu menu) {
                super(context);
                putValue(Action.NAME, "Remove Border");
                menu.addMenuListener(this);
        }

        @Override
        // Function called if we select the menu
        public void menuSelected(MenuEvent ignore) {
                boolean isDecoratorSelected = false;
                for (Figure f: context.getView().getSelection()) {
                        if (f instanceof BorderDecorator) {
                                isDecoratorSelected = true;
                                break;
                        }
                }
                setEnabled(isDecoratorSelected);
        }

        @Override
        public void actionPerformed(ActionEvent ignore) {
                for (Figure f: context.getView().getSelection()) {
                        Figure undecF = ((BorderDecorator) f).getInner();
                        //  Remove border decorator as observer from figure
                        undecF.removeFigureListener((FigureListener) f);
                        // Remove border decorator from model
                        context.getModel().removeFigure(f);
                        // Remove border decorator from selection
                        context.getView().removeFromSelection(f);
                        // Add privious figure
                        context.getModel().addFigure(undecF);
                        context.getView().addToSelection(undecF);
                }
        }
}
