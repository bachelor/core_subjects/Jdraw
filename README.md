# Jdraw
Is the JAVA framework provided for the course [Software Design FS2017 - ETHZ](http://www.vorlesungsverzeichnis.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=113676&semkez=2017S&ansicht=KATALOGDATEN&lang=de) 
in order to implement a simple graphical text editor.
It consists of multiple programing exercises that use various software design patters s.a. state, stategy, dependcy injection, factory, and many more.
## Running the editor
It can be either run:
* using Eclipse
* as a gradle project
1. `gradle build`
2. `gradle run`  


Attention: make sure to use SDK 7.
For a summary of the course check out my [TODO:summary]()  
(For study purposes only as many sections have been blantly taken over from literature)


Copyright (c) 2017 Fachhochschule Nordwestschweiz (FHNW)
All Right Reserved.
